# Book Manager
An online bookmark website using Flask and Vue. Inspired by this tutorial: https://testdriven.io/blog/developing-a-single-page-app-with-flask-and-vuejs/

**Future features and next steps:**
- Deploy the application
- Add pages read (as a button) - the main function 
- Add input verification
- Add users interface 
- Add SQLite so that each user will have their own list of books
